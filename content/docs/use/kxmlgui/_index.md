---
title: "Getting Started with KDE Frameworks"
linkTitle: "Getting Started"
weight: 2
description: >
  Discover KDE Frameworks and start building your first classic KDE desktop application.
group: "getting-started"
aliases:
  - /docs/getting-started/
---

This tutorial will guide you in creating a small text editor using
[KTextEditor](docs:ktexteditor;index.html)
, [KXmlGui](docs:kxmlgui;index.html) for
the window and [KIO](docs:kio;index.html)
for saving and loading files.

You will need to set up your development environment (so that you can use the KDE Frameworks) first. You can do that in two ways:
- Go through the [setting up your development environment](https://community.kde.org/Get_Involved/development) part of the *Get Involved* documentation. That will give you the necessary development tools and underlying libraries, and build the KDE Frameworks from scratch.
- Install the KDE Frameworks development packages from your Operating System or Distribution. The names of these packages, and how to install them, varies per distro, so you will need to investigate on your own.
